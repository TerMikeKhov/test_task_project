# Сервис уведомлений Fabrique
# Notification Service



## Описание

Cервис управления рассылками API администрирования и получения статистики.

### Пользовательский функционал проекта:

Cервис позволяет отправлять сообщения клиентам. Клиент отправляет данные клиентов и данные по рассылке.
Исходя из параметров рассылки сервис формирует перечень адресатов (клиентов) которые удовлетворяет заданным фильтрам (фильтрация по тегам и коду сотового оператора).

## Использованные технологии

* Python 3.7
* Django 2.2.19
* Django Rest Framework 3.12.4
* SQLite
* Celery
* Redis

### Запуск проекта локально

Клонировать репозиторий и перейти в него в командной строке:

```
git clone https://gitlab.com/TerMikeKhov/test_task_project.git
```

```
cd test_task_project/
```

Cоздать и активировать виртуальное окружение:

```
python3 -m venv venv
```

```
source venv/bin/activate
```

Установить зависимости из файла requirements.txt:

```
python3 -m pip install --upgrade pip
```

```
pip install -r requirements.txt
```

Перейти в директорию api-приложения

```
cd notification_service/
```

Выполнить миграции:

```
python3 manage.py migrate
```

Запустить Redis в Docker контейнере или локально:
```
redis-server
```

Запустить сервер разработки Django:
```
python3 manage.py runserver
```

Запустить Celery контейнере или локально:
```
python3 -m celery -A notification_service worker -l info
```

## Примеры запросов к API и ответов.

### Эндпоинт для отправки, изменения и удаления данных клиента (POST, PATCH, DELETE).

```
http://127.0.0.1:8000/api/clients/
```

#### Пример POST и PATCH

Request:
```
{
    "phone_number": 71111111111,
    "tags": [
        "tag3",
        "tag4",
        "tag1"
    ],
    "time_zone": "00:03:00"
}
```

Responses:
```
{
    "id": 23,
    "phone_number": 71111111111,
    "mobile_operator_code": {
        "id": 4,
        "code": 111
    },
    "tags": [
        {
            "id": 3,
            "name": "tag3"
        },
        {
            "id": 4,
            "name": "tag4"
        },
        {
            "id": 6,
            "name": "tag1"
        }
    ],
    "time_zone": "00:03:00"
}
```

#### Пример DELETE

Для удаления необходимо в URL добавить ID клиента и отправить запрос DELETE :
 ```
http://127.0.0.1:8000/api/clients/23/
 ```

 Response:  204 No Content


### Эндпоинт для создания, редакирования и удаления рассылки (POST, PATCH, DELETE).

```
http://127.0.0.1:8000/api/distribution/
```

#### Пример POST и PATCH

Request:

```
{
    "start_data_time": "2018-11-28T23:55",
    "messege": "New",
    "end_data_time": "2023-11-28T23:55",
    "properties_filter": {
        "tags": ["tag1", "tag3", "tag"],
        "codes": [111, 888]
    }
}
```

 Response:

 ```
{
    "id": 29,
    "start_data_time": "2018-11-28T23:55:00Z",
    "messege": {
        "text": "New",
        "sending_status": "dispatch not assigned"
    },
    "end_data_time": "2021-11-28T23:55:00Z",
    "properties_filter": {
        "mobile_operator_codes": [
            4,
            5
        ],
        "tags": [
            3,
            6,
            8
        ]
    }
}
 ```

#### Пример DELETE

для удаления необходимо в URL добавить ID рассылки и отправить запрос DELETE:
 ```
http://127.0.0.1:8000/api/distribution/23/
 ```

 Response:  204 No Content

 ### Эндпоинт для получения статистики по отправкам (GET). 

 ```
http://127.0.0.1:8000/api/distribution/totalstatistic/
```

 Response:

  ```
[
    {
        "messege__sending_status": "dispatch not assigned",
        "status": 17
    },
    {
        "messege__sending_status": "process sending",
        "status": 12
    }
]
```


 ### Эндпоинт для получения статистики по конкретной отправке (GET). 

 В URL необходимо указать ID отправки. В ниже приведенном случае этот ID=4.

 ```
http://127.0.0.1:8000/api/distribution/33/statistic/
```

 Response:

  ```
[
    {
        "status": "dispatch not assigned",
        "count_status": 4
    },
    {
        "status": "dispatched",
        "count_status": 10
    },
    {
        "status": "sending error",
        "count_status": 4
    },
    {
        "status": "canceled (end of time)",
        "count_status": 1
    }
]
```

 ### Эндпоинт для запуска в обработку активных рассылок и отправки сообщений клиентам (GET).


 ```
http://127.0.0.1:8000/api/start_distribution/
```

 Response: 200OK