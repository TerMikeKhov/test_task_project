class CustomBadRequestsException(Exception):
    """Raised when response.status_code != http.HTTPStatus.OK"""
    pass
