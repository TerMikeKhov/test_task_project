from rest_framework import serializers
from django.shortcuts import get_object_or_404
from datetime import datetime

from messenger.models import (
    Client, MobileOperatorCode, Tag, Distribution,
    Messege, PropertiesFilter, PropertiesFilterTag,
    PropertiesFilterMobileOperatorCode,
    STATUS_MAX_LENGTH
)
from .validators import tags_validator
from .services import (
    add_tags_to_client, send_meseges_if_possible, add_tags_to_distribution,
    add_codes_to_distribution, add_client_by_properties_filter
)


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name',)
        model = Tag


class MobileOperatorCodeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'code',)
        model = MobileOperatorCode


class TagsToClientSerilizer(serializers.Field):
    def to_representation(self, value):
        serializer = TagSerializer(value, many=True)
        return serializer.data

    def to_internal_value(self, data):
        return data


class ClientSerializer(serializers.ModelSerializer):
    tags = TagsToClientSerilizer()
    mobile_operator_code = MobileOperatorCodeSerializer(read_only=True)

    def validate_tags(self, value):
        return tags_validator(self, value)

    class Meta:
        model = Client
        fields = (
            'id', 'phone_number', 'mobile_operator_code',
            'tags', 'time_zone'
        )
        read_only_fields = ('mobile_operator_code',)

    def create(self, validated_data):
        code = int(str(validated_data.get('phone_number'))[1:4])
        mobile_operator_code, _ = MobileOperatorCode.objects.get_or_create(
            code=code)
        tags = validated_data.pop('tags')
        client = Client.objects.create(
            mobile_operator_code=mobile_operator_code, **validated_data)
        if tags:
            add_tags_to_client(client, tags)
        return client

    def update(self, instance, validated_data):
        instance.phone_number = validated_data.get(
            'phone_number', instance.phone_number)
        instance.time_zone = validated_data.get(
            'time_zone', instance.time_zone
        )
        instance.save()
        tags = validated_data.pop('tags')
        request = self.context.get('request')
        client_id = request.parser_context.get('kwargs').get('pk')
        client = get_object_or_404(Client, id=client_id)
        code = int(str(validated_data['phone_number'])[1:4])
        mobile_operator_code, _ = MobileOperatorCode.objects.get_or_create(
            code=code)
        client.mobile_operator_code = mobile_operator_code
        if tags:
            add_tags_to_client(client, tags)
        return client


class PropertiesFilterSerilizer(serializers.ModelSerializer):
    class Meta:
        model = PropertiesFilter
        fields = ('mobile_operator_codes', 'tags',)

    def to_internal_value(self, data):
        return data


class MessegeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messege
        fields = ('text', 'sending_status',)

    def to_internal_value(self, data):
        return data


class DistributionSerializer(serializers.ModelSerializer):
    properties_filter = PropertiesFilterSerilizer()
    messege = MessegeSerializer()

    class Meta:
        model = Distribution
        fields = (
            'id', 'start_data_time', 'messege',
            'end_data_time', 'properties_filter'
        )

    def validate(self, data):
        if data['start_data_time'] > data['end_data_time']:
            raise serializers.ValidationError(
                "Дата окончания рассылки должа быть не позже даты начала"
            )
        return data

    def create(self, validated_data):
        messege_text = validated_data.pop('messege')
        properties_filter = validated_data.pop('properties_filter')
        tags = properties_filter.get('tags')
        codes = properties_filter.get('codes')
        messege = Messege.objects.create(text=messege_text)
        distribution = Distribution.objects.create(
            messege=messege, **validated_data
        )
        properties_filter = PropertiesFilter.objects.create(
            distribution=distribution
        )
        if tags:
            add_tags_to_distribution(properties_filter, tags)
        if codes:
            add_codes_to_distribution(properties_filter, codes)
        if tags or codes:
            add_client_by_properties_filter(properties_filter, distribution)
        send_meseges_if_possible(distribution)
        return distribution

    def update(self, instance, validated_data):
        instance.start_data_time = validated_data.get(
            'start_data_time', instance.start_data_time)
        messege_text = validated_data.get('messege')
        instance.end_data_time = validated_data.get(
            'end_data_time', instance.end_data_time)
        instance.save()
        properties_filter_dict = validated_data.get('properties_filter')
        request = self.context.get('request')
        distribution_id = request.parser_context.get('kwargs').get('pk')
        distribution = get_object_or_404(Distribution, id=distribution_id)
        if messege_text:
            messege = Messege.objects.create(text=messege_text)
            distribution.messege = messege
        properties_filter = distribution.properties_filter
        if properties_filter_dict:
            tags = properties_filter_dict.get('tags')
            codes = properties_filter_dict.get('codes')
            if tags:
                PropertiesFilterTag.objects.filter(
                    properties_filter=properties_filter
                    ).all().delete()
                add_tags_to_distribution(properties_filter, tags)
            if codes:
                PropertiesFilterMobileOperatorCode.objects.filter(
                    properties_filter=properties_filter
                    ).all().delete()
                add_codes_to_distribution(properties_filter, codes)
            add_client_by_properties_filter(properties_filter, distribution)
        send_meseges_if_possible(distribution)
        return distribution


class TotalDistributionStatisticsSerializer(serializers.Serializer):
    messege__sending_status = serializers.CharField(
        max_length=STATUS_MAX_LENGTH
    )
    status = serializers.IntegerField()


class DistributionStatisticsSerializer(serializers.Serializer):
    status = serializers.CharField(max_length=STATUS_MAX_LENGTH)
    count_status = serializers.IntegerField()
