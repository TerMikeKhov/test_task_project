from datetime import datetime

from django.shortcuts import get_object_or_404
from django.db.models import QuerySet

from messenger.models import (
    ClientTag, Client, Tag, DistributionСlient, PropertiesFilterTag,
    MobileOperatorCode, PropertiesFilterMobileOperatorCode, PropertiesFilter,
    Distribution,
    AWAITING_DISPATCH, DISPATCHED
)
from .tasks import send_messege


def add_tags_to_client(client: Client, tags: list[str]) -> None:
    """Добавляет деги в объект клиента."""
    client_tags_object_list = []
    for tag_name in tags:
        tag, status = Tag.objects.get_or_create(name=tag_name)
        client_tags_object_list.append(ClientTag(tag=tag, client=client))
    ClientTag.objects.bulk_create(client_tags_object_list)


def add_tags_to_distribution(
    properties_filter: PropertiesFilter,
    tags: list[str]
) -> None:
    """Добавляет теги в фильтр рассылки."""
    properties_tags_object_list = []
    for tag_name in tags:
        tag = get_object_or_404(Tag, name=tag_name)
        properties_tags_object_list.append(PropertiesFilterTag(
            tag=tag, properties_filter=properties_filter))
    PropertiesFilterTag.objects.bulk_create(properties_tags_object_list)


def add_codes_to_distribution(
    properties_filter: PropertiesFilter,
    codes: list[int]
) -> None:
    """Добавляет коды телефонных операторов в фильтр рассылки."""
    distribution_codes_object_list = []
    for code in codes:
        code = get_object_or_404(MobileOperatorCode, code=code)
        distribution_codes_object_list.append(
            PropertiesFilterMobileOperatorCode(
                mobile_operator_code=code,
                properties_filter=properties_filter
            )
        )
    PropertiesFilterMobileOperatorCode.objects.bulk_create(
        distribution_codes_object_list)


def add_client_by_properties_filter(
    properties_filter: PropertiesFilter,
    distribution: Distribution
) -> None:
    """Связывает с рассылкой клиентов которые подходят под фильтры."""
    properties_filter_tags = properties_filter.tags.all()
    properties_filter_codes = properties_filter.mobile_operator_codes.all()
    if properties_filter_tags or properties_filter_codes:
        distribution.distribution_client.all().delete()
        clients = Client.objects.all()
        if properties_filter_tags:
            clients = clients.filter(
                tags__in=properties_filter_tags
                ).distinct()
        if properties_filter_codes:
            clients.filter(
                mobile_operator_code__in=properties_filter_codes
                ).distinct()
        client_distribution_object_list = []
        for client in clients.all():
            client_distribution_object_list.append(
                DistributionСlient(
                    client=client, distribution=distribution
                )
            )
        DistributionСlient.objects.bulk_create(client_distribution_object_list)


def send_meseges_if_possible(distribution: Distribution) -> None:
    """Подготавливает список для расссылки и вызывает рассылку этого списка."""
    current_datetime = datetime.now().replace(tzinfo=None)
    start_data_time = distribution.start_data_time.replace(tzinfo=None)
    end_data_time = distribution.end_data_time.replace(tzinfo=None)
    activ_distributions_clients = distribution.distribution_client.exclude(
        status=DISPATCHED)
    if activ_distributions_clients and current_datetime < end_data_time:
        if current_datetime >= start_data_time:
            prepare_to_distribution(activ_distributions_clients)


def prepare_to_distribution(
    activ_distributions_clients: QuerySet,
    start_data_time: datetime = None
) -> None:
    """Запускает через Celery асинхронную рассылку.
    Если время старта еще не наступило - создает отложенный таск,
    который будет выполнен по наступлению времени старта рассылки."""
    activ_distributions_clients.update(status=AWAITING_DISPATCH)
    for distribution_client in activ_distributions_clients.all():
        distribution_client_id = distribution_client.id
        messege_text = distribution_client.distribution.messege.text
        messege_id = distribution_client.distribution.messege.id
        client_phone = distribution_client.client.phone_number
        if start_data_time:
            send_messege.delay(
                messege_text, messege_id, client_phone,
                distribution_client_id
            )
        send_messege.apply_async(
            (messege_text, messege_id, client_phone, distribution_client_id),
            eta=start_data_time
        )
