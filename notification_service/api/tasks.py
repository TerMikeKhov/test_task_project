import requests
import http

from django.conf import settings
from django.utils import timezone

from notification_service.celery import app
from messenger.models import (
    CANCELED_END_OF_TIME, SENDING_ERROR, DISPATCHED, DistributionСlient
)
from .exceptions import CustomBadRequestsException


TOKEN = "Bearer " + settings.DISPATCH_SERVICE_JWT_TOKEN
HEADERS = {
    "Authorization": TOKEN
}


@app.task(bind=True, default_retry_delay=60)
def send_messege(
    self, messege_text, messege_id, client_phone, distribution_client_id
):
    distribution_client_object = DistributionСlient.objects.get(
        id=distribution_client_id
    )
    end_data_time = distribution_client_object.distribution.end_data_time
    current_datetime = timezone.now()
    print(end_data_time)
    if end_data_time <= current_datetime:
        distribution_client_object.status = CANCELED_END_OF_TIME
        distribution_client_object.save()
        return distribution_client_object.status
    data = {
        'id': messege_id,
        'phone': client_phone,
        'text': messege_text
    }
    try:
        response = requests.post(
            f'https://probe.fbrq.cloud/v1/send/{messege_id}',
            headers=HEADERS,
            data=data
        )
        if response.status_code != http.HTTPStatus.OK:
            distribution_client_object.status = SENDING_ERROR
            distribution_client_object.save()
            raise CustomBadRequestsException
        distribution_client_object.status = DISPATCHED
        return distribution_client_object.status
    except CustomBadRequestsException as exc:
        raise self.retry(exc=exc)
