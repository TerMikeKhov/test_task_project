from django.urls import include, path
from rest_framework.routers import DefaultRouter

from api.views import ClientViewSet, DistributionViewSet


app_name = 'api'

router = DefaultRouter()
router.register('clients', ClientViewSet, basename='clients')
router.register('distribution', DistributionViewSet, basename='distribution')

urlpatterns = [
    path('', include(router.urls)),
]
