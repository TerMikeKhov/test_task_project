from rest_framework import serializers


def tags_validator(self, value):
    is_list = isinstance(value, list)
    if not is_list:
        raise serializers.ValidationError('Должен быть список!')
    for i in value:
        is_int = isinstance(i, str)
        if not is_int:
            raise serializers.ValidationError(
                'Имя тега должно быть текстом!')
        if len(i) > 15:
            raise serializers.ValidationError(
                'Имя тега не должно составлять более 15 символов')
    return value
