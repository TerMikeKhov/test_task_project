from datetime import datetime

from django.db.models import Count
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404

from messenger.models import (
    Client, Distribution, DistributionСlient, DISPATCHED
)
from .serializers import (
    ClientSerializer, DistributionSerializer,
    TotalDistributionStatisticsSerializer, DistributionStatisticsSerializer
)
from .services import prepare_to_distribution


class ClientViewSet(viewsets.ModelViewSet):
    """
    Вьюкласс доступа к ресурсу с данными о клиентах.
    Позволяет добавление, удаление, обновление данных клиента.
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    http_method_names = ['post', 'patch', 'delete']


class DistributionViewSet(viewsets.ModelViewSet):
    """
    Вьюкласс доступа к ресурсу с от отправках.
    Позволяет добавление, удаление, обновление данных о рассылке.
    Позволяет получить статистику по всем рассылкам и по конкретной.
    Позволяет запустить обработку активных рассылок и отправку
    сообщений клиентам.
    """
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer
    http_method_names = ['get', 'post', 'patch', 'delete']

    @action(detail=False, methods=['get'])
    def totalstatistic(self, request):
        """
        Собирает и отдает клиенту статистику по всем рассылкам.
        """
        total_distribution_statistics = Distribution.objects.all().values(
            'messege__sending_status').annotate(
            status=Count('messege__sending_status'))
        serializer = TotalDistributionStatisticsSerializer(
            total_distribution_statistics, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def statistic(self, request, pk):
        """
        Собирает и отдает клиенту статистику по конкретной рассылке.
        """
        distribution = get_object_or_404(Distribution, id=pk)
        distribution_client = distribution.distribution_client.all()
        distribution_statistics = distribution_client.values(
            'status').annotate(count_status=Count('status'))
        serializer = DistributionStatisticsSerializer(
            distribution_statistics, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['get'])
    def start_distribution(self, request):
        """
        Запускает обработку активных рассылок и отправку сообщений клиентам.
        """
        current_datetime = datetime.now()
        activ_distributions_clients = DistributionСlient.objects.filter(
            distribution__start_data_time__lte=current_datetime,
            distribution__end_data_time__gt=current_datetime
        ).exclude(status=DISPATCHED)
        prepare_to_distribution(activ_distributions_clients)
        return Response(status=status.HTTP_200_OK)
