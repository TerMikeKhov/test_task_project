from django.contrib import admin

from .models import (
    Client, MobileOperatorCode, Tag, Distribution, ClientTag,
    DistributionСlient, Messege, PropertiesFilter, PropertiesFilterTag,
    PropertiesFilterMobileOperatorCode
)


admin.site.register(Client)
admin.site.register(MobileOperatorCode)
admin.site.register(Tag)
admin.site.register(Distribution)
admin.site.register(Messege)
admin.site.register(PropertiesFilter)
admin.site.register(PropertiesFilterTag)
admin.site.register(PropertiesFilterMobileOperatorCode)
admin.site.register(ClientTag)
admin.site.register(DistributionСlient)
