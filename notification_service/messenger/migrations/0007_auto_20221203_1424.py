# Generated by Django 2.2.19 on 2022-12-03 14:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messenger', '0006_auto_20221203_1252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone_number',
            field=models.IntegerField(unique=True),
        ),
    ]
