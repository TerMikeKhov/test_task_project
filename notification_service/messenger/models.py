from django.db import models

from .validators import validate_phone_number, validate_operator_code


DISPATCH_NOT_ASSIGNED = 'dispatch not assigned'
AWAITING_DISPATCH = 'awaiting dispatch'
DISPATCHED = 'dispatched'
PROCESS_SENDING = 'process sending'
CANCELED_END_OF_TIME = 'canceled (end of time)'
SENDING_ERROR = 'sending error'
STATUSES = (
    (DISPATCH_NOT_ASSIGNED, 'dispatch not assigned'),
    (AWAITING_DISPATCH, 'awaiting dispatch'),
    (DISPATCHED, 'dispatched'),
    (PROCESS_SENDING, 'process sending'),
    (CANCELED_END_OF_TIME, 'canceled (end of time)'),
    (SENDING_ERROR, 'sending error')
)
STATUS_MAX_LENGTH = max(len(status[1]) for status in STATUSES)


class Tag(models.Model):
    name = models.CharField(
        verbose_name='Имя тега',
        help_text='Укажите имя тега',
        db_index=True,
        unique=True,
        max_length=15,
    )

    def __str__(self):
        return self.name


class MobileOperatorCode(models.Model):
    code = models.PositiveSmallIntegerField(
        verbose_name='Код мобильного оператора',
        help_text='Укажите код мобильного оператора',
        db_index=True,
        unique=True,
        validators=[validate_operator_code]
    )

    def __str__(self):
        return self.code


class Client(models.Model):
    phone_number = models.IntegerField(
        unique=True,
        validators=[validate_phone_number]
    )
    mobile_operator_code = models.ForeignKey(
        MobileOperatorCode,
        on_delete=models.CASCADE,
        related_name='client',
        verbose_name='Код мобильного оператора',
    )
    tags = models.ManyToManyField(
        Tag,
        through='ClientTag',
        verbose_name='Теги',
        help_text='Выберите теги',
        blank=True,
    )
    time_zone = models.DurationField(
        verbose_name='Часовой пояс',
        help_text='Укажите часовой пояс',
    )

    def __str__(self):
        return self.phone_number


class Messege(models.Model):
    date_time_of_creation_dispatch = models.DateTimeField(
        verbose_name='Дата и время создания отправки',
        auto_now_add=True,
    )
    sending_status = models.CharField(
        choices=STATUSES,
        default=DISPATCH_NOT_ASSIGNED,
        verbose_name='Статус отправки',
        max_length=STATUS_MAX_LENGTH,
    )
    text = models.TextField(
        'Текст сообщения',
        help_text='Введите текст сообщения'
    )

    def __str__(self):
        return self.id


class Distribution(models.Model):
    start_data_time = models.DateTimeField(
        verbose_name='Дата и время запуска рассылки',
        help_text='Укажите дату и время запуска рассылки',
    )
    messege = models.ForeignKey(
        Messege,
        on_delete=models.CASCADE,
        related_name='distribution',
        verbose_name='Сообщение',
    )
    end_data_time = models.DateTimeField(
        verbose_name='Дата и время окончания рассылки',
        help_text='Укажите дату и время окончания рассылки',
    )

    def __str__(self):
        return self.id


class DistributionСlient(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='distribution_client',
        verbose_name='Клиент сообщения',
    )
    status = models.CharField(
        choices=STATUSES,
        default=DISPATCH_NOT_ASSIGNED,
        verbose_name='Статус отправки',
        max_length=STATUS_MAX_LENGTH,
    )
    distribution = models.ForeignKey(
        Distribution,
        on_delete=models.CASCADE,
        related_name='distribution_client',
        verbose_name='Клиент сообщения',
    )

    class Meta:
        constraints = (
            models.UniqueConstraint(
                fields=('client', 'distribution',),
                name='unique_client_distribution'
            ),
        )

    def __str__(self):
        return f'Messege {self.distribution} for {self.client}'


class ClientTag(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.id


class PropertiesFilter(models.Model):
    distribution = models.OneToOneField(
        Distribution,
        on_delete=models.CASCADE,
        related_name='properties_filter',
    )
    tags = models.ManyToManyField(
        Tag,
        through='PropertiesFilterTag',
        verbose_name='Теги',
        help_text='Выберите теги',
        blank=True
    )
    mobile_operator_codes = models.ManyToManyField(
        MobileOperatorCode,
        through='PropertiesFilterMobileOperatorCode',
        verbose_name='Теги',
        help_text='Выберите теги',
        blank=True
    )

    def __str__(self):
        return self.id


class PropertiesFilterTag(models.Model):
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
    )
    properties_filter = models.ForeignKey(
        PropertiesFilter,
        on_delete=models.CASCADE,
    )

    class Meta:
        constraints = (
            models.UniqueConstraint(
                fields=('properties_filter', 'tag',),
                name='unique_tag'
            ),
        )

    def __str__(self):
        return self.id


class PropertiesFilterMobileOperatorCode(models.Model):
    mobile_operator_code = models.ForeignKey(
        MobileOperatorCode,
        on_delete=models.CASCADE,
    )
    properties_filter = models.ForeignKey(
        PropertiesFilter,
        on_delete=models.CASCADE,
    )

    class Meta:
        constraints = (
            models.UniqueConstraint(
                fields=('mobile_operator_code', 'properties_filter',),
                name='unique_code'
            ),
        )

    def __str__(self):
        return self.id
