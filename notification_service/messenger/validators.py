from django.core.exceptions import ValidationError


def validate_phone_number(value):
    if len(str(value)) != 11:
        raise ValidationError('Должно быть 11 цифр в номере телефона!')


def validate_operator_code(value):
    if len(str(value)) != 3:
        raise ValidationError('Код операторая должен состоять из 3 цифр!')
